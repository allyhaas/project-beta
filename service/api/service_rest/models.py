from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=200)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Appointment(models.Model):
    vin = models.CharField(max_length=200, default="")
    customer_name = models.CharField(max_length=200)
    date = models.DateField()
    time = models.TimeField()
    technician = models.ForeignKey(
        "Technician",
        related_name="models",
        on_delete=models.CASCADE,
    )
    reason = models.CharField(max_length=200, default="")
    vip_status = models.CharField(max_length=100, default="")
    status = models.CharField(max_length=100, default="INCOMPLETE")
