import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';


function ManufacturerList () {
    const [manufacturers, setManufacturers] = useState([])
    const fetchData = async () => {
        const ManufacturerUrl = 'http://localhost:8100/api/manufacturers/'
        const response = await fetch(ManufacturerUrl)
        if (response.ok) {
            const data = await response.json()
            setManufacturers(data.manufacturers)
        }
    }


    const deleteData = async (id) => {
        const response = await fetch(`http://localhost:8100/api/manufacturers/${id}/`, {method: "DELETE"})
    }


    const navigate = useNavigate()
    const directToForm = () => {
        navigate("new/")
    }


    useEffect(() => {
        fetchData()
    }, [])
    

    return <>
        <br />
        <h1>Manufacturer List</h1>
        <br />
        <div id="manufacturer_list">
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>id</th>
                    <th>manufacturer name</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {manufacturers?.map(manufacturer => {
                    return (
                        <tr>
                            <td>{ manufacturer.id }</td>
                            <td>{manufacturer.name }</td>
                            <td><button onClick={() => deleteData(manufacturer.id)} style={{ backgroundColor: "red", color: "white" }} className="btn btn-outline-danger">delete</button></td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        </div>
        <button onClick={directToForm} style={{ backgroundColor: "blue", color: "white" }}> Add a new manufacturer</button>
    </>
}

export default ManufacturerList
