import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';


function TechnicianList () {

    const [technicians, setTechnicians] = useState([])

    const fetchData = async () => {
        const url = "http://localhost:8080/api/technicians/"
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setTechnicians(data.technicians)
        }
    }

    const deleteData = async (id) => {
        const response = await fetch(`http://localhost:8080/api/technicians/${id}`, {method: 'DELETE'})
      }

    const navigate = useNavigate()

    const directToForm = () => {
        navigate("new/")
    }


    useEffect(() => {
        fetchData()
    }, [])

    
    return <>
        <br />
        <h1>Technicians List</h1>
        <br />
        <div id="technicians_list">
        <table className='table table-striped'>
            <thead>
                <tr>
                    <th>emoloyee name</th>
                    <th>employee number</th>
                </tr>
            </thead>
            <tbody>
                {technicians.map(technician => {
                    return (
                        <tr>
                            <td>{ technician.name }</td>
                            <td>{ technician.employee_number }</td>
                            <td><button onClick={() => deleteData(technician.id)} style={{ backgroundColor: "red", color: "white" }} className="btn btn-outline-danger">delete</button></td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        </div>
        <button onClick={directToForm} style={{ backgroundColor: "blue", color: "white" }}> Add a new technician</button>
    </>
}

export default TechnicianList
