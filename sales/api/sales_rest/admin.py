from django.contrib import admin
from .models import SalesRecord, Employee, Customer, AutomobileVO

# Register your models here.
@admin.register(SalesRecord)
class SalesRecord(admin.ModelAdmin):
    pass

@admin.register(Employee)
class Employee(admin.ModelAdmin):
    pass

@admin.register(Customer)
class Customer(admin.ModelAdmin):
    pass

@admin.register(AutomobileVO)
class AutomobileVO(admin.ModelAdmin):
    pass
