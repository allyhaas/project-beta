from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import SalesRecord, Employee, Customer, AutomobileVO
from .encoders import AutomobileVOEncoder, EmployeeEncoder, CustomerEncoder, SalesEncoder

#WORKS
@require_http_methods(["GET"])
def list_automobiles(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobiles": automobiles},
            encoder=AutomobileVOEncoder,
        )

#WORKS
@require_http_methods(["DELETE"])
def delete_automobiles(request,pk):
        try:
            automobiles = AutomobileVO.objects.get(id=pk)
            automobiles.delete()
            return JsonResponse(
                automobiles,
                encoder=AutomobileVOEncoder,
                safe=False,
            )
        except AutomobileVO.DoesNotExist:
            return JsonResponse({"message":"That Automobile Doesn't Exist"})


#GET WOULD WORK, BUT NEED TO FIX POST REQUEST...
@require_http_methods(["GET", "POST"])
def list_sales(request):
    if request.method == "GET":
        sales_record = SalesRecord.objects.all()
        return JsonResponse(
            {"sales": sales_record},
            encoder=SalesEncoder,
            safe=False
        )

#POST STARTS HERE:
    else:
        content = json.loads(request.body)

        customer_id=content["sales_customer_id"]
        customer =Customer.objects.get(id=customer_id)
        content["sales_customer_id"] = customer_id
        print(customer_id)

        employee_id = content["sales_employee_id"]
        employee = Employee.objects.get(id=employee_id)
        content["sales_employee_id"] = employee_id
        print(employee_id)

        automobile_id = content["automobile_id"]
        automobile = AutomobileVO.objects.get(id=automobile_id)
        content["automobile_id"] = automobile_id
        print (automobile_id)

        sales_record = SalesRecord.objects.create(**content)
        return JsonResponse(
            sales_record,
            encoder=SalesEncoder,
            safe=False
        )



#DELETE WOULD WORK, GET THROWS 500 ERROR
@require_http_methods(["DELETE", "GET"])
def show_sale(request, pk):
    if request.method == "GET":
        try:
            sale = SalesRecord.objects.get(id=pk)
            return JsonResponse(
                {"sale": sale},
                encoder=SalesEncoder
            )
        except SalesRecord.DoesNotExist:
            response = JsonResponse({"message" : "No Sales Record"})
            response.status_code = 404,
            return response
    elif request.method == "DELETE":
        try:
            sale = SalesRecord.objects.get(id=pk)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SalesEncoder,
                safe=False,
            )
        except SalesRecord.DoesNotExist:
            return JsonResponse({"message": "No Sales Record"})


#WORKS
@require_http_methods(["GET", "POST"])
def list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )

##WORKS
@require_http_methods(["GET", "DELETE"])
def show_customer(request, pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                {"customer": customer},
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message":"Does Not Exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message":"Does Not Exist"})

##WORKS
@require_http_methods(["GET", "POST"])
def list_employees(request):
    if request.method == "GET":
        employees = Employee.objects.all()
        return JsonResponse(
            {"employees": employees},
            encoder=EmployeeEncoder
        )
    else:
        content = json.loads(request.body)
        employee = Employee.objects.create(**content)
        return JsonResponse(
            employee,
            encoder=EmployeeEncoder,
            safe=False,
        )

##WORKS
@require_http_methods(["GET", "DELETE"])
def show_employee(request, pk):
    if request.method == "GET":
        try:
            employee = Employee.objects.get(id=pk)
            return JsonResponse(
                {"employee": employee},
                encoder=EmployeeEncoder,
                safe=False
            )
        except Employee.DoesNotExist:
            response = JsonResponse({"message":"Does Not Exist"})
            response.status_code = 404,
            return response
    elif request.method == "DELETE":
        try:
            employee = Employee.objects.get(id=pk)
            employee.delete()
            return JsonResponse(
                employee,
                encoder=EmployeeEncoder,
                safe=False
            )
        except Employee.DoesNotExist:
            return JsonResponse ({"message" : "Does Not Exist"})
